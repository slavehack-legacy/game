<?php
	use \Auth\User;

	class Auth extends \Slim\Middleware {
		/**
		 * Slim middleware hook that sets up the user and admin objects in templates
		 */
		public function call() {
			$user = null;

			if (isset($_SESSION['user'])) {
				$user = new User($_SESSION['user']);
				$this->app->view()->setData('user', $user);
			}

			if (isset($_SESSION['admin'])) {
				$admin = new User($_SESSION['admin']);
				$this->app->view()->setData('admin', $admin);
			}

			$this->next->call();
		}

		/**
		 * Authenticate an admin for a role
		 * @param {string} $role Required role to access route
		 * @return {function} Slim middleware
		 */
		static function admin($role) {
			return function() use ($role) {
				global $app;

				$admin = $app->view()->getData('admin');

				if ((!$admin && $role == 'nobody')
				   ||($admin && $admin->hasRole($role)))
					return true;

				$app->notFound();
			};
		}

		/**
		 * Authenticate a user for a role
		 * @param {string} $role Required role to access route
		 * @param {string} [$redirect] Location to redirect if the user does not have a role
		 * @return {function} Slim middleware
		 */
		static function role($role, $redirect=false) {
			return function() use ($role, $redirect) {
				global $app;

				$user = $app->view()->getData('user');

				if ((!$user && $role == 'nobody')
				   ||($user && $user->hasRole($role)))
					return true;

				if (!$redirect)
					$app->notFound();

				if ($user)
					$app->redirect('/');

				$app->flash('error', 'Sorry, you need to log in to do that!');
				$_SESSION['redirect'] = $app->request()->getPath();
				$app->redirect($redirect);
			};
		}

		/**
		 * Authenticate a user based on billing status
		 * @param {string} $plan Required plan to access route
		 * @param {string} [$redirect] Location to redirect if the user does not have a plan
		 * @return {function} Slim middleware
		 */
		static function plan($plan, $redirect=false) {
			return function() use ($role) {
				global $app;

				$user = $app->view()->getData('user');

				if (($plan == 'paid' || $plan == 'unpaid')
				   &&$user && $user->hasRole('paid') == ($plan == 'paid'))
					return true;

				if ((!$user && $plan == 'p_none')
				   ||($user && $user->hasRole("p_$plan")))
					return true;

				if (!$redirect)
					$app->notFound();

				if ($user)
					$app->redirect('/');

				$app->flash('error', 'Sorry, you need a different plan to access that!');
				$_SESSION['redirect'] = $app->request()->getPath();
				$app->redirect($redirect);
			};
		}

		/**
		 * Attempt to create a new user with the given account information
		 * @param {string} $username
		 * @param {string} $password
		 * @param {string} $email
		 * @param {string} [$confirmPassword]
		 * @param {string} [$confirmEmail]
		 * @return {array|bool|\Auth\User} Returns a new \Auth\User() on success, an array of validation errors, or false if an unknown error has occurred
		 */
		static function register($username, $password, $email, $confirmPassword = false, $confirmEmail = false) {
			$error = [];

			if (($e = User::validateName($username)) !== true)
				$error['login'] = $e;

			if (($e = User::validateEmail($email)) !== true)
				$error['email'] = $e;

			if (($e = User::validatePassword($password)) !== true)
				$error['password'] = $e;

			if ($confirmPassword !== false && $confirmPassword != $password)
				$error['confirm-password'] = 'match';

			if ($confirmEmail !== false && $confirmEmail != $email)
				$error['confirm-email'] = 'match';

			if (count($error))
				return $error;

			if (!($user = User::create($username, $email)))
				return false;

			$user->updatePassword($password);
			return $user;
		}

		/**
		 * Validate a user's login credentials
		 * @param {string} $login User's email address or username
		 * @param {string} $password User's password
		 * @return {bool|\Auth\User} Returns a new \Auth\User if credentials match, false if credentials do not match
		 */
		static function login($login, $password) {
			if (!($user = User::findByName($login) ?: User::findByEmail($login)))
				return false;

			if (!$user->comparePassword($password))
				return false;

			$user->updateLoginDate();

			return $user;
		}
	}
