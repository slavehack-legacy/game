<?php
	use \Slim\Slim;

	class Scheme extends \Slim\Middleware {
		const HTTP = 1;
		const HTTPS = 2;
		const ANY = 3;

		static $scheme = 3;
		static $forced = false;
		static $preferred = 'http';

		private static function schemeAllowed($scheme) {
			return !!($scheme == 'http' ? self::$scheme & self::HTTP : self::$scheme & self::HTTPS);
		}

		public static function redirect($scheme) {
			$app = Slim::getInstance();
			$req = $app->request();
			$currentScheme = $req->getScheme();
			$url = substr($req->getUrl(), strlen($currentScheme));
			$path = $req->getPath();

			if ($app->request()->getIp() != '127.0.0.1' &&
			    $shcheme != $currentScheme &&
			    self::schemeAllowed($scheme)) {
				$res = $app->response();
				$res['Location'] = "$scheme$url$path";
			}
		}

		private static function force($scheme) {
			$app = Slim::getInstance();

			return function() use ($scheme, $app) {
				Scheme::redirect($scheme);
			};
		}

		public static function http() {
			return self::force('http');
		}

		public static function https() {
			return self::force('https');
		}

		public function call() {
			$scheme = $this->app->request()->getScheme();

			if (!self::schemeAllowed($scheme))
				self::redirect($scheme == 'http' ? 'https' : 'http');

			$this->next->call();

			$preferred = self::$preferred == 'https' ? 'https' : 'http';

			if (!self::$forced && self::$preferred && $scheme != $preferred)
				self::redirect($preferred);
		}
	}
