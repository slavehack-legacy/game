CREATE DATABASE `slavehack` DEFAULT CHARACTER SET utf8;

USE `slavehack`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stripe_id` varchar(20) DEFAULT NULL,
  `login` varchar(30) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `hash` varchar(123) NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email_confirmed` boolean NOT NULL DEFAULT FALSE,
  `mailing_list` boolean NOT NULL DEFAULT FALSE,
  `temporary_password` boolean NOT NULL DEFAULT FALSE,
  `locked` boolean NOT NULL DEFAULT FALSE,
  `active` boolean NOT NULL DEFAULT FALSE,
  `gravatar` boolean NOT NULL DEFAULT TRUE,
  `referrer` int(10) unsigned DEFAULT NULL,
  `roles` varchar(128) NOT NULL DEFAULT 'user',
  `status` varchar(32) DEFAULT NULL,
  `signature` text,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `stripe_id_UNIQUE` (`stripe_id`),
  KEY `referrer` (`referrer`),
  CONSTRAINT `user_referrer` FOREIGN KEY (`referrer`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `logs` (
  `timestamp` datetime NOT NULL,
  `message` varchar(128) DEFAULT NULL,
  `type` varchar(16) NOT NULL,
  `user` int(10) unsigned DEFAULT NULL,
  `admin` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `plans` (
  `stripe_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `currency` varchar(3) NOT NULL DEFAULT 'usd',
  `interval` varchar(5) NOT NULL DEFAULT 'month',
  `interval_count` int(10) unsigned NOT NULL DEFAULT '1',
  `trial_period_days` int(10) unsigned DEFAULT NULL,
  `livemode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stripe_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
