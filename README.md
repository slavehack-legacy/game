 Slavehack [![Donations][]][Gratipay]
===========
This is a complete rewrite of Slavehack.  Check out the [wiki][] and [issue
tracker][], and feel free to open issues.  The goal of this project is to
replicate all existing functionality cleanly before adding new functionality.

Visit #legacy on irc.slavehack.com to chat.

[Slavehack]: http://www.slavehack.com/
[wiki]: https://bitbucket.org/slavehack-legacy/game/wiki
[issue tracker]: https://bitbucket.org/slavehack-legacy/game/issues
[Gratipay]: https://gratipay.com/slavehack
[Donations]: http://img.shields.io/gratipay/slavehack.svg

## Dependencies
- PHP 5.4+
- [Composer](https://getcomposer.org/download/)

## Quickstart
Copy `config.sample.json` to `config.json`, then install dependencies with
composer:

```
php -r "readfile('https://getcomposer.org/installer');" | php
php composer.phar install
```

Congrats!  Dependencies are installed.  Schema info is floating in
(schema/)[schema/].  Import the .sql files into MySQL or MariaDB using your
favorite SQL admin tool.

Now all that's left is to run the server:

```
php -t public -S 127.0.0.1:5000
```

Then point your browser over to http://localhost:5000/.  That's it, it's running.
