<?php
	$app->post('/login', \Scheme::https(), \Auth::role('nobody'), function() use ($app) {
		$req = $app->request();

		$go    = $req->get('go');

		$login = $req->post('login');
		$pass  = $req->post('password');

		if (!($user = \Auth::login($login, $pass))) {
			$app->flashNow('error', 'Invalid login/password combination');
			$app->flashNow('login', $login);
			$app->pass();
		}

		if ($user->locked && !$user->hasRole('admin')) {
			$app->flashNow('error', 'Your account has been disabled');
			$app->pass();
		}

		$_SESSION['user'] = $user->id;

		if ($user->hasRole('admin') || $user->hasRole('super'))
			$_SESSION['admin'] = $user->id;

		$app->flash('success', 'Logged in!');
		$app->redirect($_SESSION['redirect'] ?: $go ? "/$go" : '/');
	});

	$app->map('/login', \Scheme::https(), \Auth::role('nobody'), function() use ($app) {
		$req = $app->request();
		$go  = $req->get('go');

		$header = $req->headers();

		$host = ($header['HTTPS'] ? 'https' : 'http') . "://{$header['HOST']}/";
		$referer = substr("{$header['REFERER']}/", 0, strlen($host));

		if ($go && $referer !== $host)
			$app->redirect('/login');

		$app->render('auth/login.html');
	})->via('GET', 'POST');
