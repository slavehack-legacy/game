<?php
	$app->post('/register', \Scheme::https(), \Auth::role('nobody'), function() use ($app, $config) {
		$req = $app->request();

		$login = $req->post('login');

		$email = $req->post('email');

		$password = $req->post('password');
		$confirmPassword = $req->post('confirm-password');

		$plan = $req->post('plan');

		$response = \Auth::register($login, $password, $email, $confirmPassword);

		if (!is_bool($response) && !is_array($response)) {
			$user = $response;
			$user->mailing_list = (bool) $req->post('mailing-list');
			$user->commit();

			$_SESSION['user'] = $user->id;
			$user->updateLoginDate();

			$user->sendEmail('registered.html', [
				'token' => $user->getToken('confirm.email')
			]);

			if (!$config['site']['billing']['showPlansOnRegister'])
				$plan = false;

			if ($app->view->getData('stripePubKey'))
				\Auth\Customer::create($user, $plan);

			$app->flash('success', 'Thanks for registering!');
			$app->redirect('/');
			return;
		}

		$app->flashNow('login', $login);
		$app->flashNow('email', $email);
		$app->flashNow('plan', $plan);

		if (is_array($response)) {
			foreach ($response as $k => $v)
				$app->flashNow("error.$k", $v);
		} else {
			$app->flashNow('error', 'An unknown error has occurred');
		}

		$app->pass();
	});

	$app->map('/register', \Scheme::https(), \Auth::role('nobody'), function() use ($app, $config) {
		$plans = [];

		if ($config['site']['billing']['showPlansOnRegister'])
			$plans = \Auth\Customer::plans();

		$app->render('auth/register.html', [
			'plans' => $plans
		]);
	})->via('GET', 'POST');
