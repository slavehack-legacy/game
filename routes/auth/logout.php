<?php
	$app->post('/logout', \Auth::role('user', '/login'), function() use ($app) {
		if ($_SESSION['admin'] && $_SESSION['admin'] != $_SESSION['user']) {
			$_SESSION['user'] = $_SESSION['admin'];
			$app->flash('notice.admin-logout', true);
			$app->redirect('/');
		}

		session_destroy();
		session_start();
		$app->flash('info', 'You have logged out');
		$app->redirect('/login');
	});


