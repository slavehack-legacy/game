<?php
	$app->get('/:page?', function($page = 'home') use ($app) {
		if (file_exists($app->config('templates.path') . "/.pages/$page.html"))
			$app->render(".pages/$page.html");
		else
			$app->pass();
	})
	->conditions([ 'page' => '[a-z0-9-]+' ]);
?>
