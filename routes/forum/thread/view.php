<?php
	use \Forum\Forum;

	$app->get('/forum/:category/:thread(/p:page)', function($category, $thread, $page=0) use ($app, $db) {
		$user     = $app->view()->getData('user');
		$thread   = Forum::thread($thread);
		$category = Forum::category($category);

		if (!$thread || !$category)
			$app->notFound();

		$postsPerPage = 5;
		$app->render('forum/thread/view.html', array(
			'category'   => $category['id'],
			'forum'      => $category,
			'categories' => Forum::categories(),
			'posts'      => Forum::posts($user, $thread['id'], $page, $postsPerPage),
			'thread'     => $thread,
			'page'       => $page,
			'pages'      => floor(max(0, Forum::getPostCount($thread['id']) - 1) / $postsPerPage)
		));
	})
	->conditions($forum_conditions);
