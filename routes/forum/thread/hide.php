<?php
	use \Forum\Forum;

	$app->get('/forum/:category/:thread/hide', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, [ 'hidden' => 1 ]);
		$app->flash('info', 'Made thread hidden.');
		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);

	$app->get('/forum/:category/:thread/unhide', \Auth::role('forum mod'), function($category, $thread) use ($app) {
		Forum::updateThread($thread, [ 'hidden' => 0 ]);
		$app->flash('info', 'Thread no longer hidden.');
		$app->redirect("/forum/$category/$thread");
	})
	->conditions($forum_conditions);
