<?php
	use \Forum\Forum;

	$globalRoles = ((array) $globalRoles) + [
		'staff' => 'Can post to invisible forums/topics',
		'forum mod' => 'Can delete forum posts/topics, edit posts, post to readonly forums/topics',
		'forum admin' => 'Can edit forum categories, undelete posts/topics',
	];

	// Middleware-y bit
	$app->map('/forum(/:foo+)', function() use ($app) {
		if ($user = $app->view()->getData('user')) {
			if ($user->hasRole('staff') || $user->hasRole('forum mod'))
				Forum::$showHidden = true;

			if ($user->hasRole('forum admin'))
				Forum::$showDeleted = true;
		}

		$app->pass();
	})->via('GET', 'POST');
