<?php
	use \Forum\Forum;

	$app->get('/forum(/(:category))(/p:page)', function($category=0, $page=0) use ($app, $db) {
		$cat = Forum::category($category);

		if (!$cat && $category != 0)
			$app->notFound();

		$threadsPerPage = 25;
		$app->render('forum/view.html', array(
			'category'   => $category,
			'forum'      => $cat,
			'categories' => Forum::categories(),
			'threads'    => Forum::threads($category, $page),
			'page'       => $page,
			'pages'      => floor(max(0, Forum::getThreadCount($category) - 1) / $threadsPerPage)
		));
	})
	->conditions($forum_conditions);
