<?php
	$forum_conditions = [
		'page'     => '[0-9]+',
		'category' => '[0-9]+',
		'thread'   => '[0-9]+',
		'post'     => '[0-9]+',
		'action'   => 'edit|reply',
	];
