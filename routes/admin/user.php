<?php
	$app->post('/admin/user/:id', \Auth::admin('admin'), function($id) use ($app, $db) {
		global $globalRoles;

		$result = \Auth\User::findById($id);
		$admin = $app->view()->getData('admin');
		$req = $app->request();

		$result->first_name = $req->post('first_name');
		$result->last_name = $req->post('last_name');

		$result->email = $req->post('email');

		$result->email_confirmed = !!$req->post('email_confirmed');
		#$result->mailing_list = !!$req->post('mailing_list');
		$result->gravatar = !!$req->post('gravatar');

		$result->locked = !!$req->post('locked');

		$result->notes = $req->post('notes');

		if ($admin->hasRole('super')) {
			$roles = array_merge([
				'admin' => 'Can access admin panel',
				'super' => 'Can access user accounts, assign roles',
			], (array) $globalRoles);

			foreach ($roles as $role => $v) {
				if ($req->post('role-' . str_replace(' ', '-', $role)))
					$result->addRole($role);
				else if ($result->id != $admin->id || ($role != 'admin' && $role != 'super'))
					$result->removeRole($role);
			}
		}

		$result->commit();

		$app->pass();
	});

	$app->map('/admin/user/:id', \Auth::admin('admin'), function($id) use ($app, $db, $globalRoles) {
		global $globalRoles;

		$result = \Auth\User::findById($id);
		$admin = $app->view()->getData('admin');

		$roles = (array) $globalRoles;

		if ($admin->hasRole('super')) {
			$roles = array_merge([
				'admin' => 'Can access admin panel',
				'super' => 'Can access user accounts, assign roles',
			], $roles);
		}

		$app->render('admin/user/view.html', [
			'roles' => $roles,
			'result' => $result,
			'gravatar' => md5($result->email),
		]);
	})->via('GET', 'POST');
