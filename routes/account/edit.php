<?php
	use \Auth\User;

	$app->get('/account/edit', \Auth::role('user'), function () use ($app) {
		$user = $app->view()->getData('user');
		$flash = $app->view()->getData('flash');

		if (!$flash['email'])
			$app->flashNow('email', $user->email);

		if (!$flash['mailing-list'])
			$app->flashNow('mailing-list', $user->mailing_list);

		$app->render('account/edit.html');
	});

	$app->post('/account/edit', \Auth::role('user'), function () use ($app) {
		$req = $app->request();
		$user = $app->view()->getData('user');

		$currentPassword = $req->post('current-password');

		$email = $req->post('email');

		$mailingList = $req->post('mailing-list');

		$password = $req->post('password');
		$confirmPassword = $req->post('confirm-password');

		$error = [];

		if (!$user->comparePassword($currentPassword)) {
			if (strlen($currentPassword) == 0)
				$error['current-password'] = 'required';
			else
				$error['current-password'] = 'incorrect';
		}

		if (strlen($password) || strlen($confirmPassword)) {
			if (is_string($e = User::validatePassword($password)))
				$error['password'] = $e;

			if ($confirmPassword != $password)
				$error['confirm-password'] = 'match';
		}

		if ($email != $user->email && is_string($e = User::validateEmail($email)))
				$error['email'] = $e;

		if (count($error)) {
			$app->flash('email', $email);
			$app->flash('mailing-list', $mailingList);

			foreach ($error as $t => $e)
				$app->flash("error.$t", $e);
		} else {
			$user->mailing_list = (bool) $mailingList;

			if ($email != $user->email) {
				$user->updateEmail($email);
				$app->flash('success.email', 'changed');
			}

			if ($password) {
				$user->updatePassword($password);
				$app->flash('success.password', 'changed');
			}

			$user->commit();

			$app->flash('success', 'Changes saved!');
		}

		$app->redirect('/account/edit#');
	});
