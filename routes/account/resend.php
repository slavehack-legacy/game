<?php
	$app->get('/account/resend-confirmation', \Auth::role('user'), function() use ($app) {
		$user = $app->view()->getData('user');
		if ($user->email_confirmed) {
			$app->flash('error', 'Your email address has already been confirmed');
		} else {
			$user->sendEmail('registered.html', [
				'token' => $user->getToken('confirm.email')
			]);
			$app->flash('success', 'Your confirmation email has been re-sent');
		}
		$app->redirect('/account');
	});
