<?php
	use \Auth\User;

	$app->get('/account', \Auth::role('user'), function () use ($app) {
		$user = $app->view()->getData('user');
		$flash = $app->view()->getData('flash');

		if (!$flash['email'])
			$app->flashNow('email', $user->email);

		if (!$flash['mailing-list'])
			$app->flashNow('mailing-list', $user->mailing_list);

		$app->render('account/view.html', [
			'avatar' => md5($user->email)
		]);
	});
